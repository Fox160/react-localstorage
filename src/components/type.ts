export interface IJSONData {
  data: IJSONDataItem[]
}

export interface IJSONDataItem {
  question: string,
  id: number
}

export interface IPropsInput {
  initialValue: string,
  handleInputChange: any,
  className?: string,
}

export interface IFormData {
  [key: string]: string
}

export type inputChangeType = {
  name: string,
  value: string
}