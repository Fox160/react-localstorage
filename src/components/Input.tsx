import React, { useState } from 'react';
import { useEffect } from 'react';
import { IJSONDataItem, IPropsInput } from './type';

export default function Input(props: (IPropsInput & IJSONDataItem)) {
  const [inputValue, setInputValue] = useState(props.initialValue || "");

  useEffect(() => {
    setInputValue(props.initialValue || "");

    return () => {
      setInputValue("");
    }
  }, [props.initialValue])

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    setInputValue(value);
    props.handleInputChange({ name, value });
  };

  return (
    <div className={`input ${props.className}`}>
      <label className="input__label" htmlFor={props.id.toString(10)}>
        {props.question}
      </label>
      <input
        id={props.id.toString(10)}
        name={props.question}
        onChange={handleChange}
        value={inputValue}
        placeholder="*Введите текст"
        className="input__field"
      />
    </div>
  );
}
