import React, { SyntheticEvent, useState } from 'react';
import jsonData from '../data.json';
import Input from './Input';
import { IFormData, inputChangeType } from './type';

export default function Form() {
  const [formData, setFormData] = useState<IFormData>(setLocalStorageData || {});

  function setLocalStorageData () {
    const localStorageData = localStorage.getItem("formData");

    if (typeof localStorageData === 'string') {
      try {
        return JSON.parse(localStorageData);
      } catch (e) {
        return false;
      }
    }
  }

  const handleInputChange = ({name, value} : inputChangeType) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleFormSubmit = (e: SyntheticEvent) => {
    e.preventDefault();

    if (Object.keys(formData).length === jsonData.data.length) {
      setFormData({});
      localStorage.setItem('formData', JSON.stringify(formData));
    }
  };

  return (
    <form className="form" onSubmit={handleFormSubmit}>
      {jsonData.data.map((item, index) => {
        return (
          <Input
            {...item}
            key={item.id || index}
            className="form__input"
            handleInputChange={handleInputChange}
            initialValue={formData[item.question] || ''}
          />
        );
      })}
      <button type="submit" className="form__submit">
        Оправить
      </button>
    </form>
  );
}
